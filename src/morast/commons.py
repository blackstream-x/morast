# -*- coding: utf-8 -*-

"""

morast.commons

Common constants


Copyright (C) 2024 Rainer Schwarzbach

This file is part of morast.

morast is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

morast is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


#
# Module constants
#

BLANK = " "
DOT = "."
EMPTY = ""
LF = "\n"
POUND = "#"
UNDERSCORE = "_"

BRAND = "Morast"
PYPI_URL = "https://pypi.org/project/morast/"
UTF8 = "utf-8"

SRC = "src"
DOCS = "docs"
REFERENCE = "reference"
MORAST_CONFIG_DIR = ".morast"
OVERRIDES = "overrides"
CONFIG_FILE_NAME = "config.yaml"

# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
