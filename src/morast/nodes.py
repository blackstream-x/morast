# pylint: disable=too-many-lines
# -*- coding: utf-8 -*-

"""

morast.nodes

Documentation nodes generated from a module’s abstract syntax tree


Copyright (C) 2024 Rainer Schwarzbach

This file is part of morast.

morast is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

morast is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""

import ast
import datetime
import logging
import re

from typing import Any, Dict, List, Optional, Type, Union

from smdg import elements as mde
from smdg import strings as mds

from morast import __version__
from morast import commons

#
# Constants
#


ARGS_JOINER = ", "
EQUALS_OP = "="
ASSIGNMENT_WIDE = f" {EQUALS_OP} "

OPENING_CURLY_BRACE = "{"
CLOSING_CURLY_BRACE = "}"
OPENING_PARENTHESIS = "("
CLOSING_PARENTHESIS = ")"
OPENING_SQUARE_BRACKET = "["
CLOSING_SQUARE_BRACKET = "]"

MD_ASSIGNMENT_NARROW = mde.InlineElement(f"\N{ZERO WIDTH JOINER}{EQUALS_OP}")
MD_ASSIGNMENT_WIDE = mde.InlineElement(ASSIGNMENT_WIDE)
MD_DICT_ITEM_JOINER = mde.InlineElement(": ")
MD_ANNOTATION_JOINER = mde.InlineElement(": ")
MD_DOT = mde.InlineElement(".")
MD_STAR = mde.InlineElement("*")
MD_RETURN_ARROW = mde.InlineElement(" → ")
MD_ARGS_JOINER = mde.InlineElement(ARGS_JOINER)
MD_HR20 = mde.HorizontalRule(20)
MD_HR60 = mde.HorizontalRule(60)

LINENO = "lineno"
UNKNOWN = "<unknown>"

__all__ = [
    "MorastBinOp",
    "MorastDict",
    "MorastKeyword",
    "MorastCall",
    "MorastConstant",
    "MorastFormattedValue",
    "MorastJoinedStr",
    "MorastIfExp",
    "MorastList",
    "MorastComprehension",
    "MorastListComp",
    "MorastName",
    "MorastNamespace",
    "MorastStarred",
    "MorastSubscript",
    "MorastTuple",
    "MorastBaseNode",
    "MorastErrorNode",
]

OP_LOOKUP: Dict[Type, str] = {
    ast.Add: "+",
    ast.Sub: "+",
    ast.Mult: "*",
    ast.Div: "/",
    ast.FloorDiv: "//",
    ast.Mod: "%",
    ast.Pow: "**",
    ast.LShift: "<<",
    ast.RShift: ">>",
    ast.BitOr: "|",
    ast.BitXor: "^",
    ast.BitAnd: "&",
    # TODO:  ast.MatMult: "@",
}


#
# Functions
#


def failsafe_mde(item: Any) -> mde.BaseElement:
    """Return a MarkDown element
    from an item of any type
    """
    if isinstance(item, MorastBaseNode):
        return item.as_markdown()
    #
    return mde.InlineElement(str(item))


def get_operator_text(operator_obj: ast.AST) -> str:
    """Return the string representation matching
    _operator\\_obj_ (an ast [ast binary operator token] instance):
    `+`, `-`, `*`, `/`, etc
    """
    return OP_LOOKUP[type(operator_obj)]


def get_augmentation_operator(operator_obj: ast.AST) -> str:
    """Return the string representation matching
    _operator\\_obj_ (an [ast binary operator token] instance
    used in an [ast.AugAssign] instance)
    (`+=`, `-=`, `*=`, `/=`, etc)
    """
    return f"{get_operator_text(operator_obj)}="


def get_node(element: Union[str, ast.AST]) -> "MorastBaseNode":
    """Return a Morast document Node
    for the provided _element_ which may be a string
    or an ast object.
    """
    # pylint: disable=too-many-branches
    # pylint: disable=too-many-return-statements
    if isinstance(element, str):
        # name
        return MorastName(element)
    #
    if isinstance(element, ast.Name):
        # name
        return MorastName(element.id)
    #
    if isinstance(element, ast.Attribute):
        # namespaced name
        return MorastNamespace(get_node(element.value), get_node(element.attr))
    #
    if isinstance(element, ast.BinOp):
        # Binary operator
        return MorastBinOp(get_node(element.left), element.op, get_node(element.right))
    #
    if isinstance(element, ast.Constant):
        # constant / literal
        return MorastConstant(element.value)
    #
    if isinstance(element, ast.Dict):
        # Dict representation
        return MorastDict(element)
    #
    if isinstance(element, ast.keyword):
        # keyword argument without annotation
        arg = getattr(element, "arg", None)
        return MorastKeyword(get_node(element.value), arg=arg)
    #
    if isinstance(element, ast.comprehension):
        return MorastComprehension(element)
    #
    if isinstance(element, ast.ListComp):
        return MorastListComp(element)
    #
    if isinstance(element, ast.FormattedValue):
        # formatted value inside a format string
        conversion = getattr(element, "conversion", -1)
        format_spec = getattr(element, "format_spec", None)
        format_spec_items: List[str] = []
        if format_spec:
            format_spec_items.extend(str(get_node(item.value)) for item in format_spec)
        #
        return MorastFormattedValue(
            get_node(element.value), conversion, *format_spec_items
        )
    #
    if isinstance(element, ast.JoinedStr):
        # Format string consisting of literals and formatted values
        parts: List[Union[MorastConstant, MorastFormattedValue]] = []
        for item in element.values:
            current_node = get_node(item)
            if isinstance(current_node, (MorastConstant, MorastFormattedValue)):
                parts.append(current_node)
            else:
                return MorastErrorNode(
                    f"Source line {element.lineno}:"
                    f" {ast.dump(item)} (in {ast.dump(element)})"
                    " not implemented yet"
                )
            #
        #
        return MorastJoinedStr(*parts)
    #
    if isinstance(element, ast.IfExp):
        return MorastIfExp(
            get_node(element.test),
            get_node(element.body),
            get_node(element.orelse),
        )
    #
    if isinstance(element, (ast.List, ast.Set, ast.Tuple)):
        # List, Set or Tuple literal
        params: List[MorastBaseNode] = [get_node(item) for item in element.elts]
        if isinstance(element, ast.List):
            target_class: Type[MorastBaseNode] = MorastList
        if isinstance(element, ast.Set):
            target_class = MorastSet
        else:
            target_class = MorastTuple
        #
        return target_class(*params)
    #
    if isinstance(element, ast.Starred):
        # Sinsle starred argument
        return MorastStarred(get_node(element.value))
    #
    if isinstance(element, ast.Subscript):
        # Subscripted object
        return MorastSubscript(get_node(element.value), get_node(element.slice))
    #
    if isinstance(element, ast.Call):
        # function call
        params = []
        for arg_or_kw in element.args + element.keywords:
            params.append(get_node(arg_or_kw))
        #
        return MorastCall(get_node(element.func), *params)
    #
    lineno = getattr(element, LINENO, UNKNOWN)
    return MorastErrorNode(
        f"Source line {lineno}:" f" {ast.dump(element)} not implemented yet"
    )


def remove_hanging_indent(text: str, level: int = 1) -> str:
    """Return a text block with hanging indent removed,
    which is used in conversion of docstrings.

    *   _text_: the source text
    *   _level_: the number of indent levels to decrease.
        Default is 1 (suitable for module-level classes and functions);
        for any type of methods, 2 is recommended choice.
        Values lower than 1 return the text block unchanged.

    One index unit is counted as 4 Spaces or one Tab.

    Extra indent is preserved, see _line 3_ in the following example:

        >>> remove_hanging_indent(
        ...     "line 1\\n    line 2\\n        line 3\\n    line 4"
        ... )
        'line 1\\nline 2\\n    line 3\\nline 4'
        >>>

    """
    if level < 1:
        return text
    #
    prx_hanging_indent = re.compile(f"^(    |\\t){{{level}}}")
    fixed_lines: List[str] = []
    for index, line in enumerate(text.splitlines(keepends=True)):
        if not index:
            fixed_lines.append(line)
            continue
        #
        fixed_lines.append(prx_hanging_indent.sub(commons.EMPTY, line))
    #
    return commons.EMPTY.join(fixed_lines)


#
# Classes
#


class MorastBaseNode:
    """Base class for all Morast nodes"""

    def __init__(self) -> None:
        """init method to be overridden in subclasses"""

    def __repr__(self) -> str:
        """Return as string representation"""
        return f"<{self.__class__.__name__} instance>"

    @property
    def quotes_required(self) -> bool:
        """Return True in subclasses if quotes are required"""
        return False

    def plain_display(self) -> str:
        """Return the plain display of the element"""
        as_str = str(self)
        if self.quotes_required:
            return repr(as_str)
        #
        return as_str

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown element repesenting the node.
        If not overridden, this method simlpy returns a
        MarkDown inline element from the string conversion of the object.
        """
        return mde.InlineElement(self.plain_display())


# pylint: disable=too-few-public-methods


class MorastErrorNode(MorastBaseNode):
    """Error element

    Initialization argument:

    * _message_: the error message which is stored as a public attribute
    """

    def __init__(self, message: str) -> None:
        """Store _message_ and log a warning"""
        logging.warning(message)
        self.message = message

    def as_markdown(self) -> mde.BaseElement:
        """Return the error message in a MarkDown paragraph"""
        return mde.Paragraph(self.message)


class MorastKeyword(MorastBaseNode):
    """Keyword argument from ast

    Initialization arguments:

    * _value_: the argument value
      (or the name to be prefixed with stars if arg is `None`)
    * _arg_: the argument name
    """

    def __init__(self, value: MorastBaseNode, arg: Optional[str] = None) -> None:
        """Store _value_ and _arg_"""
        self._value = value
        self._arg = arg

    @property
    def prefix(self) -> str:
        """Prefix for the value
        (`arg=` or `**`), epending on the value of arg
        """
        if self._arg is None:
            return "**"
        #
        return f"{self._arg}="

    def __str__(self) -> str:
        """String conversion:
        either `arg=value` or `**value` if arg is `None`
        """
        return f"{self.prefix}{self._value}"


class MorastConstant(MorastBaseNode):
    """Literal constant from ast

    Initialization argument:

    * _value_: the value
    """

    def __init__(self, value: Any) -> None:
        """Store the value"""
        self._value = value

    @property
    def quotes_required(self) -> bool:
        """Quotes are required for string values"""
        return isinstance(self._value, str)

    def __str__(self) -> str:
        """String conversion"""
        if self._value is Ellipsis:
            return "…"
        #
        return str(self._value)

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown CodeSpan element
        from the string conversion of the value.
        For string values, use the _representation_
        of the string (ie. the string enclosed in quotes).
        For the special value _Ellipsis_, return
        an inline element containing representation as **…**
        """
        if self._value is Ellipsis:
            return mde.InlineElement(str(self))
        #
        return mde.CodeSpan(self.plain_display())


class MorastFormattedValue(MorastBaseNode):
    """Formatted value in an f-string

    Initialization arguments:

    * _value_: a MorastBaseNode subclass instance
      representing the value
    * _conversion_: the conversion character ASCII code as documented in
      <https://docs.python.org/3/library/ast.html#ast.FormattedValue>
    * _format\\_spec\\_items: the parts of the format\\_spec attribute
      (converted to strings before)
    """

    def __init__(self, value: MorastBaseNode, conversion: int, *format_spec_items: str):
        """Store the values"""
        self._value = value
        modifier_parts: List[str] = []
        if conversion > -1:
            modifier_parts.append(f"!{chr(conversion)}")
        #
        if format_spec_items:
            modifier_parts.append(f":{commons.EMPTY.join(format_spec_items)}")
        #
        self._modifiers = commons.EMPTY.join(modifier_parts)

    @property
    def quotes_required(self) -> bool:
        """Quotes are required."""
        return True

    def __str__(self) -> str:
        """Return as string"""
        return f"{self._value}{self._modifiers}"

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown CodeSpan from the string representation"""
        return mde.CodeSpan(self.plain_display())


class MorastJoinedStr(MorastBaseNode):
    """An f-string

    Initialization arguments:

    * _parts_: the parts of the f-string
      (a sequence of MorastConstant and/or MorastFormattedValue
      instances)
    """

    def __init__(
        self,
        *parts: Union[MorastConstant, MorastFormattedValue],
    ):
        """Store the parts"""
        self._parts = parts

    @property
    def quotes_required(self) -> bool:
        """Quotes are required."""
        return True

    def __str__(self) -> str:
        """Return as string (parts simply joined together)"""
        output_parts: List[str] = []
        for part in self._parts:
            if isinstance(part, MorastFormattedValue):
                output_parts.append(f"{{{part}}}")
            else:
                output_parts.append(str(part))
            #
        #
        return commons.EMPTY.join(output_parts)

    def plain_display(self) -> str:
        """Return the plain display of the element"""
        return f"f{super().plain_display()}"

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown CodeSpan element from the representation string"""
        return mde.CodeSpan(self.plain_display())


class MorastIfExp(MorastBaseNode):
    """IfExp operator from ast"""

    def __init__(
        self,
        test: MorastBaseNode,
        body: MorastBaseNode,
        orelse: MorastBaseNode,
    ) -> None:
        """Set internal values"""
        self._test = test
        self._body = body
        self._orelse = orelse

    def __str__(self) -> str:
        """Return as string"""
        return f"{self._body} if {self._test} else {self._orelse}"


class MorastBinOp(MorastBaseNode):
    """Binary operator from ast"""

    def __init__(
        self,
        left: MorastBaseNode,
        operator_obj: ast.AST,
        right: MorastBaseNode,
    ) -> None:
        """Set the operator representation"""
        self._operator = f" {get_operator_text(operator_obj)} "
        self.left = left
        self.right = right

    def __str__(self) -> str:
        """Return as string"""
        return f"{self.left}{self._operator}{self.right}"

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown element"""
        return mde.CompoundInlineElement(
            self.left.as_markdown(),
            mde.InlineElement(self._operator),
            self.right.as_markdown(),
        )


class MorastComprehension(MorastBaseNode):
    """Coomprehension from ast"""

    def __init__(self, comp: ast.comprehension) -> None:
        """Set the name"""
        self._target = get_node(comp.target)
        self._iter = get_node(comp.iter)
        self._ifs: List[MorastBaseNode] = [
            get_node(single_if) for single_if in comp.ifs
        ]
        self._async_prefix = "async " if comp.is_async else commons.EMPTY

    def __str__(self) -> str:
        """Return as string"""
        ifs_suffixes: List[str] = []
        for single_if in self._ifs:
            ifs_suffixes.append(f" {single_if}")
        #
        return (
            f"{self._async_prefix}for {self._target} in {self._iter}"
            f"{commons.EMPTY.join(ifs_suffixes)}"
        )


class MorastBaseComp(MorastBaseNode):
    """all types of comprehensions from ast"""

    opener = OPENING_SQUARE_BRACKET
    closer = CLOSING_SQUARE_BRACKET

    def __init__(
        self,
        first_part: MorastBaseNode,
        *generators: MorastComprehension,
    ) -> None:
        """Store the first part and the generators"""
        self._first_part = first_part
        self._generators = generators

    def __str__(self) -> str:
        """Return as string"""
        gen_suffixes: List[str] = []
        for single_gen in self._generators:
            gen_suffixes.append(f" {single_gen}")
        #
        return (
            f"{self.opener}{self._first_part}"
            f" {commons.EMPTY.join(gen_suffixes)}{self.closer}"
        )


class MorastListComp(MorastBaseComp):
    """List comprehension from ast"""

    def __init__(self, list_comp: ast.ListComp) -> None:
        """Setore the internal components: elt and generator(s)"""
        elt = get_node(list_comp.elt)
        generators: List[MorastComprehension] = [
            MorastComprehension(single_gen) for single_gen in list_comp.generators
        ]
        super().__init__(elt, *generators)


class MorastSetComp(MorastBaseComp):
    """Set comprehension from ast"""

    opener = OPENING_CURLY_BRACE
    closer = CLOSING_CURLY_BRACE

    def __init__(self, set_comp: ast.SetComp) -> None:
        """Setore the internal components: elt and generator(s)"""
        elt = get_node(set_comp.elt)
        generators: List[MorastComprehension] = [
            MorastComprehension(single_gen) for single_gen in set_comp.generators
        ]
        super().__init__(elt, *generators)


class MorastDictComp(MorastBaseComp):
    """Dict comprehension from ast"""

    opener = OPENING_CURLY_BRACE
    closer = CLOSING_CURLY_BRACE

    def __init__(self, dict_comp: ast.DictComp) -> None:
        """Setore the internal components: elt and generator(s)"""
        dict_item = MorastDictItem(dict_comp.key, dict_comp.value)
        generators: List[MorastComprehension] = [
            MorastComprehension(single_gen) for single_gen in dict_comp.generators
        ]
        super().__init__(dict_item, *generators)


class MorastName(MorastBaseNode):
    """Name from ast"""

    def __init__(self, name: str) -> None:
        """Set the name"""
        self._name = name

    def __str__(self) -> str:
        """Return as string"""
        return self._name


class MorastNamespace(MorastBaseNode):
    """Namespace(d name) from ast"""

    def __init__(self, namespace: MorastBaseNode, descendant: MorastBaseNode) -> None:
        """Set the name"""
        self._namespace = namespace
        self._descendant = descendant

    def strip_first(self) -> MorastBaseNode:
        """Return the descendant only"""
        return self._descendant

    def __str__(self) -> str:
        """Return as string"""
        return f"{self._namespace}.{self._descendant}"

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown element"""
        return mde.CompoundInlineElement(
            self._namespace.as_markdown(),
            MD_DOT,
            self._descendant.as_markdown(),
        )


class MorastDictItem(MorastBaseNode):
    """Dict item from ast"""

    def __init__(self, key: Optional[ast.AST], value: Optional[ast.AST]) -> None:
        """Set the name"""
        self._kwarg: Optional[MorastBaseNode] = None
        self._key: Optional[MorastBaseNode] = None
        self._value: Optional[MorastBaseNode] = None
        if isinstance(value, ast.AST):
            value_node = get_node(value)
        else:
            value_node = MorastBaseNode()
        #
        if key is None:
            self._kwarg = MorastKeyword(value_node)
        else:
            self._key = get_node(key)
            self._value = value_node
        #

    def __str__(self) -> str:
        """Return as string"""
        if isinstance(self._key, MorastBaseNode) and isinstance(
            self._value, MorastBaseNode
        ):
            return f"{self._key.plain_display()}:" f" {self._value.plain_display()}"
        #
        if isinstance(self._kwarg, MorastBaseNode):
            return str(self._kwarg)
        #
        raise ValueError("invalid DictItem – should not happen")

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown element"""
        if isinstance(self._kwarg, MorastBaseNode):
            return self._kwarg.as_markdown()
        #
        key = failsafe_mde(self._key)
        value = failsafe_mde(self._value)
        return mde.CompoundInlineElement(key, MD_DICT_ITEM_JOINER, value)


class MorastStarred(MorastBaseNode):
    """Starred item"""

    def __init__(
        self,
        *value: MorastBaseNode,
    ):
        """Store the value"""
        self._value = value

    def __str__(self) -> str:
        """Return as string"""
        return f"*{self._value}"


class CompoundNode(MorastBaseNode):
    """Element containing a number of other ones"""

    prefix_node: Optional[MorastBaseNode] = None
    opener: str = commons.EMPTY
    closer: str = commons.EMPTY
    joiner: str = ARGS_JOINER
    add_supported: bool = False

    def __init__(self, *contents: MorastBaseNode) -> None:
        """set name and contents"""
        self._contents = list(contents)

    def __str__(self) -> str:
        """String representation"""
        str_all: List[str] = []
        if self.prefix_node is not None:
            str_all.append(str(self.prefix_node))
        #
        if self.opener:
            str_all.append(self.opener)
        #
        str_all.extend(self.get_contents())
        if self.closer:
            str_all.append(self.closer)
        #
        return commons.EMPTY.join(str_all)

    def add(self, element: MorastBaseNode) -> None:
        """add the new element"""
        if self.add_supported:
            self._contents.append(element)
        else:
            raise ValueError("Adding elements not supported")
        #

    def get_contents(self) -> List[str]:
        """Return a list of strings from contents,
        including joiners
        """
        str_contents: List[str] = []
        for index, element in enumerate(self._contents):
            if index > 0 and len(self.joiner) > 0:
                str_contents.append(self.joiner)
            #
            str_contents.append(element.plain_display())
        #
        return str_contents

    def get_contents_md(self) -> List[mde.BaseElement]:
        """Return a list of MarkDown elements from contents,
        including joiners
        """
        contents_joiner = mde.InlineElement(self.joiner)
        md_contents: List[mde.BaseElement] = []
        for index, element in enumerate(self._contents):
            if index > 0 and len(contents_joiner) > 0:
                md_contents.append(contents_joiner)
            #
            md_contents.append(element.as_markdown())
        #
        return md_contents

    def get_full_md(self) -> List[mde.BaseElement]:
        """Return a list of MarkDown elements including prefix,
        opener and closer
        """
        md_all: List[mde.BaseElement] = []
        if self.prefix_node is not None:
            md_all.append(self.prefix_node.as_markdown())
        #
        if self.opener:
            md_all.append(mde.InlineElement(self.opener))
        #
        md_all.extend(self.get_contents_md())
        if self.closer:
            md_all.append(mde.InlineElement(self.closer))
        #
        return md_all

    def as_markdown(self) -> mde.BaseElement:
        """Return a compound markdown element"""
        return mde.CompoundInlineElement(*self.get_full_md())


class MorastCall(CompoundNode):
    """Function (or other callable) call from ast"""

    opener = OPENING_PARENTHESIS
    closer = CLOSING_PARENTHESIS

    def __init__(self, func: MorastBaseNode, *params: MorastBaseNode) -> None:
        """set name and contents"""
        super().__init__(*params)
        # self._func = func
        self.prefix_node = func


class MorastDict(CompoundNode):
    """Dict from ast"""

    opener = OPENING_CURLY_BRACE
    closer = CLOSING_CURLY_BRACE

    def __init__(
        self,
        ast_dict_obj: ast.Dict,
    ) -> None:
        """set name and contents"""
        dict_items: List[MorastDictItem] = []
        for index, single_key in enumerate(ast_dict_obj.keys):
            dict_items.append(MorastDictItem(single_key, ast_dict_obj.values[index]))
        #
        super().__init__(*dict_items)


class MorastSubscript(CompoundNode):
    """Subscript from ast"""

    opener = OPENING_SQUARE_BRACKET
    closer = CLOSING_SQUARE_BRACKET

    def __init__(self, value: MorastBaseNode, slice_: MorastBaseNode) -> None:
        """set name and contents"""
        super().__init__(slice_)
        # self._value = value
        self.prefix_node = value


class MorastClassBases(CompoundNode):
    """Class bases from ast"""

    opener = commons.EMPTY
    closer = commons.EMPTY

    def __init__(self, *bases, prefix: str = "") -> None:
        """set name and contents"""
        super().__init__(*(get_node(single_base) for single_base in bases))
        self.prefix_node = MorastName(f"{prefix}Inherits from: ")


class MorastTuple(CompoundNode):
    """Tuple from ast"""

    opener = OPENING_PARENTHESIS
    closer = CLOSING_PARENTHESIS

    def __init__(self, *contents: MorastBaseNode) -> None:
        """Store contents, and avoid parentheses
        if more thon one element was provided
        """
        super().__init__(*contents)
        if len(self._contents) > 1:
            self.opener = self.closer = commons.EMPTY
        #

    def get_contents(self) -> List[str]:
        """Return a list of strings from contents,
        including joiners.
        Append a trailing comma to length-1 tuples.
        """
        str_contents: List[str] = super().get_contents()
        if len(self._contents) == 1:
            str_contents.append(",")
        #
        return str_contents

    def get_contents_md(self) -> List[mde.BaseElement]:
        """Return a list of MarkDown elements from contents,
        including joiners.
        Append a trailing comma to length-1 tuples.
        """
        md_contents: List[mde.BaseElement] = super().get_contents_md()
        if len(self._contents) == 1:
            md_contents.append(mde.InlineElement(","))
        #
        return md_contents


class MorastList(CompoundNode):
    """List from ast"""

    opener = OPENING_SQUARE_BRACKET
    closer = CLOSING_SQUARE_BRACKET
    add_supported = True


class MorastSet(CompoundNode):
    """Set from ast"""

    opener = OPENING_CURLY_BRACE
    closer = CLOSING_CURLY_BRACE
    add_supported = True

    def __str__(self) -> str:
        """String representation"""
        if len(self._contents) < 1:
            return "set()"
        #
        return super().__str__()

    def get_full_md(self) -> List[mde.BaseElement]:
        """Return a list of MarkDown elements including prefix,
        opener and closer
        """
        if len(self._contents) < 1:
            return [mde.InlineElement("set()")]
        #
        return super().get_full_md()


class Assignment(MorastBaseNode):
    """Represents an assignment"""

    def __init__(
        self,
        element: Union[ast.Assign, ast.AnnAssign, ast.AugAssign],
        prefix: str = "",
    ) -> None:
        """Store the assignment parts"""
        self.target: MorastBaseNode = MorastName("...")
        self.prefix = prefix
        self.operator: Optional[str] = EQUALS_OP
        self.annotation: Optional[MorastBaseNode] = None
        self.value: Optional[MorastBaseNode] = None
        declared_value = getattr(element, "value", None)
        if declared_value is not None:
            self.value = get_node(declared_value)
        #
        self.__target_list: List[MorastBaseNode] = []
        if isinstance(element, ast.Assign):
            self.__target_list.extend(
                get_node(single_target) for single_target in element.targets
            )
            self.target = self.__target_list[0]
        elif isinstance(element, ast.AnnAssign):
            self.target = get_node(element.target)
            if declared_value is None:
                self.operator = None
            #
            self.annotation = get_node(element.annotation)
        elif isinstance(element, ast.AugAssign):
            self.target = get_node(element.target)
            self.operator = get_augmentation_operator(element.op)
        #
        if not self.__target_list:
            self.__target_list.insert(0, self.target)
        #

    def strip_first(self) -> None:
        """Strip the first namespace part from the first target"""
        if isinstance(self.target, MorastNamespace):
            self.target = self.target.strip_first()
            try:
                self.__target_list[0] = self.target
            except IndexError:
                self.__target_list.append(self.target)
            #
        #

    def __str__(self) -> str:
        """Return a MarkDown representation"""
        parts: List[str] = []
        if self.prefix:
            parts.append(self.prefix)
        #
        for index, single_target in enumerate(self.__target_list):
            if index:
                parts.append(ASSIGNMENT_WIDE)
            #
            parts.append(str(single_target))
        #
        if self.annotation is not None:
            parts.extend(
                (
                    ": ",
                    str(self.annotation),
                )
            )
        #
        if self.operator is not None:
            if isinstance(self.value, MorastBaseNode):
                parts.extend(
                    (
                        f" {self.operator} ",
                        str(self.value),
                    )
                )
            #
        #
        return commons.EMPTY.join(parts)

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown representation"""
        parts: List[mde.BaseElement] = []
        if self.prefix:
            parts.append(mde.InlineElement(self.prefix))
        #
        for index, single_target in enumerate(self.__target_list):
            if index:
                parts.append(MD_ASSIGNMENT_WIDE)
            #
            parts.append(mde.BoldText(single_target.as_markdown()))
        #
        if self.annotation is not None:
            parts.extend(
                (
                    mde.InlineElement(": "),
                    self.annotation.as_markdown(),
                )
            )
        #
        if self.operator is not None:
            if isinstance(self.value, MorastBaseNode):
                parts.extend(
                    (
                        mde.InlineElement(f" {self.operator} "),
                        self.value.as_markdown(),
                    )
                )
            #
        #
        return mde.CompoundInlineElement(*parts)


class DocString(MorastBaseNode):
    """Represents a Docstring"""

    def __init__(
        self,
        expression: Union[ast.Expr, str],
        level: int = 1,
        sanitize: bool = False,
    ) -> None:
        """Initialization arguments:

        *   _expression_: source ast.Expr or string
        *   _level_: the docstring level, one of:
            *   `0`: preformatted docstring,
                no hanging index correction necessary, immutable level
            *   `1`: usually module-level docstring,
                no hanging index correction necessary,
                but level is changeable
            *   `3`: module-level function or class docstring,
                hanging index correction by one level
            *   `4`: class or instance method,
                hanging index correction by two levels
        *   _sanitize_: Flag determining if sanitization is required
        """
        if isinstance(expression, str):
            content = expression
        elif isinstance(expression.value, ast.Constant):
            content = expression.value.value
        else:
            raise ValueError("Must be a constant expression!")
        #
        if level < 0:
            raise ValueError("level must be 0 or greater")
        #
        self._level = level
        if sanitize:
            self._sane_content = mds.sanitize(content)
        else:
            self._sane_content = mds.declare_as_safe(content)
        #

    def adjust_level(self, new_level: int) -> None:
        r"""Change the level to _new\_level_ if it may be changed"""
        if new_level < 1:
            raise ValueError("level must be 1 or greater")
        #
        if self._level > 0:
            self._level = new_level
        #

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown representation"""
        content = remove_hanging_indent(str(self._sane_content), self._level - 2)
        return mde.BlockQuote(mds.declare_as_safe(content))


class Signature(MorastBaseNode):
    """Represents the signature of a class or function"""

    # pylint: disable = too-many-arguments
    # pylint: disable=too-many-positional-arguments

    def __init__(
        self,
        name: str,
        arguments: ast.arguments,
        returns: Any,
        prefix: Optional[str] = None,
        skip_first_arg: bool = False,
    ) -> None:
        """Store the content"""
        self.name = name
        self._prefix = prefix
        self.args: List[mde.BaseElement] = []
        if arguments.posonlyargs:
            self._add_args(
                arguments.posonlyargs,
                arguments.defaults,
                defaults_offset=len(arguments.args),
                from_backwards=True,
            )
        #
        if arguments.args and arguments.posonlyargs:
            self.args.append(mde.InlineElement("/"))
        #
        self._add_args(arguments.args, arguments.defaults, from_backwards=True)
        vararg = getattr(arguments, "vararg", None)
        kwarg = getattr(arguments, "kwarg", None)
        if isinstance(vararg, ast.arg):
            self._add_starred_arg(vararg)
        elif (arguments.posonlyargs or arguments.args) and (
            arguments.kwonlyargs or kwarg
        ):
            self.args.append(MD_STAR)
        #
        # add kwonlyargs
        self._add_args(
            arguments.kwonlyargs, arguments.kw_defaults, from_backwards=False
        )
        #
        if isinstance(kwarg, ast.arg):
            self._add_starred_arg(kwarg, 2)
        #
        if skip_first_arg:
            try:
                self.args.pop(0)
            except IndexError:
                lineno = getattr(arguments, LINENO, UNKNOWN)
                logging.warning(
                    "Source line %s (%s): Skipping first arg failed",
                    lineno,
                    ast.dump(arguments),
                )
            #
        #
        self.returns: Optional[MorastBaseNode] = None
        if returns is not None:
            self.returns = get_node(returns)
        #

    def _add_starred_arg(self, arg_to_star, nstars: int = 1) -> None:
        """Add an argument to maybe-be-starred"""
        annotation = getattr(arg_to_star, "annotation", None)
        prefix_stars = [MD_STAR] * nstars
        compound_arg_parts: List[mde.BaseElement] = [
            mde.ItalicText(
                mde.CompoundInlineElement(
                    *prefix_stars, get_node(arg_to_star.arg).as_markdown()
                )
            )
        ]
        if isinstance(annotation, ast.AST):
            compound_arg_parts.extend(
                (
                    MD_ANNOTATION_JOINER,
                    get_node(annotation).as_markdown(),
                )
            )
        #
        self.args.append(mde.CompoundInlineElement(*compound_arg_parts))

    def _add_args(
        self, args, defaults, defaults_offset: int = 0, from_backwards=False
    ) -> None:
        """Add posonlyargs, args or kwonlyargs"""
        nargs = len(args) + defaults_offset
        for index, arg in enumerate(args):
            if from_backwards:
                index = index - nargs
            #
            if not isinstance(arg, ast.arg):
                if isinstance(arg, ast.AST):
                    lineno = getattr(arg, LINENO, UNKNOWN)
                    logging.warning(
                        "Source line %s (%s): Unhandled arg",
                        lineno,
                        ast.dump(arg),
                    )
                else:
                    logging.warning("Unhandled arg: %r", arg)
                #
            #
            arg_element = get_node(arg.arg).as_markdown()
            annotation = getattr(arg, "annotation", None)
            try:
                ast_default = defaults[index]
            except IndexError:
                default: Optional[MorastBaseNode] = None
            else:
                default = get_node(ast_default)
            #
            arg_element = mde.ItalicText(arg_element)
            if annotation is None and default is None:
                self.args.append(arg_element)
                continue
            #
            compound_arg_parts: List[mde.BaseElement] = [arg_element]
            if annotation is None:
                assignment_element = MD_ASSIGNMENT_NARROW
            else:
                assignment_element = MD_ASSIGNMENT_WIDE
                compound_arg_parts.extend(
                    (
                        MD_ANNOTATION_JOINER,
                        get_node(annotation).as_markdown(),
                    )
                )
            #
            if default is not None:
                compound_arg_parts.extend((assignment_element, default.as_markdown()))
            #
            self.args.append(mde.CompoundInlineElement(*compound_arg_parts))
        #

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown representation"""
        # joiner = mde.InlineElement(ARGS_JOINER)
        output_args: List[mde.BaseElement] = []
        for index, single_elem in enumerate(self.args):
            if index:
                output_args.append(MD_ARGS_JOINER)
            #
            if isinstance(single_elem, mde.CompoundElement):
                output_args.extend(single_elem.flattened())
            else:
                output_args.append(single_elem)
            #
        #
        return_annotation: List[mde.BaseElement] = []
        if self.returns is not None:
            return_annotation.extend(
                (
                    MD_RETURN_ARROW,
                    self.returns.as_markdown(),
                )
            )
        #
        name_parts: List[mde.InlineElement] = []
        if self._prefix is not None:
            name_parts.append(mde.InlineElement(self._prefix))
        #
        name_parts.append(mde.BoldText(self.name))
        # logging.warning(repr(name_parts[-1]))
        # logging.warning(str(name_parts[-1]))
        # args = ARGS_JOINER.join(self.args)
        inline_element = mde.CompoundInlineElement(
            *name_parts,
            mde.SAFE_LP,
            *output_args,
            mde.SAFE_RP,
            *return_annotation,
        )
        return mde.BlockQuote(
            MD_HR60,
            inline_element,
            MD_HR60,
        )


class Advertisement(MorastBaseNode):
    """Represents the advertisement at the end of the output"""

    def __init__(self, prefix: str = "") -> None:
        """Store the prefix"""
        self._prefix = prefix

    def as_markdown(self) -> mde.BaseElement:
        """Return a MarkDown representation"""
        today = datetime.datetime.now().strftime("%Y-%m-%d")
        return mde.BlockElement(
            MD_HR60,
            mde.Paragraph(
                mde.ItalicText(
                    mde.CompoundInlineElement(
                        f"{self._prefix} Module contents extracted ",
                        mde.BoldText(today),
                        " by ",
                        mde.BoldText(
                            mde.Link(
                                f"{commons.BRAND} v{__version__}",
                                ref="morast-pypi",
                            )
                        ),
                    )
                )
            ),
            mde.Label("morast-pypi", commons.PYPI_URL),
        )


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
