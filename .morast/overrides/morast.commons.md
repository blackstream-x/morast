# commons module

This module contains common constants used by various modules
in the package

## BLANK module-level constant

A single blank (or space) character

## DOT module-level constant

A single dot character

## EMPTY module-level constant

Empty string

## LF module-level constant

Line feed character

## POUND module-level constant

Pound character (number sign / atx-style header prefix)

## UNDERSCORE module-level constant

Underscore character

## BRAND module-level constant

"Morast" – the "brand" of this package

## PYPI_URL module-level constant

PyPI URL of this package

## UTF8 module-level constant

UTF-8 codec constant for unicode-ty-bytes encoding and vice versa.

## SRC module-level constant

The "src" subdirectory for module sources

## DOCS module-level constant

The "docs" subdirectory

## REFERENCE module-level constant

"reference" subdirectory

## MORAST_CONFIG_DIR module-level constant

".morast" subdirectory for the configuration file

## OVERRIDES module-level constant

"overrides" subdirectory in **MORAST\_CONFIG\_DIR**
for override files

## CONFIG_FILE_NAME module-level constant

Default configuration file name in **MORAST\_CONFIG\_DIR**
