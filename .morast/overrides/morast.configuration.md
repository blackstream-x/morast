# configuration module

This module is used to handle a global configuration

## MORAST_BASE_PATH module-level constant

Base path for configuration and overrides

## DEFAULT_ADVERTISE module-level constant

Default value for the **advertise** setting
(compare the **[GlobalOptions()][GlobalOptions]** class)

## DEFAULT_SOURCE module-level constant

Default value for the **source\_path** setting
(compare the **[GlobalOptions()][GlobalOptions]** class)

## DEFAULT_DESTINATION module-level constant

Default value for the **destination\_path** setting
(compare the **[GlobalOptions()][GlobalOptions]** class)

## DEFAULT_CONFIG_FILE module-level constant

Default configuration file path

## DEFAULT_OVERRIDES_BASEPATH module-level constant

Default value for the **overrides\_basepath** setting
(compare the **[GlobalOptions()][GlobalOptions]** class)

## DEFAULT_CONFIG_SOURCE module-level constant

Preset configuration source

## KW_ADVERTISE module-level constant | ignore

## KW_SOURCE_PATH module-level constant | ignore

## KW_DESTINATION_PATH module-level constant | ignore

## KW_OVERRIDES_BASEPATH module-level constant | ignore

## KW_EXCLUDED_MODULES module-level constant | ignore

## KW_CONFIG_SOURCE module-level constant | ignore

## KW_EMOJI module-level constant | ignore

## KW_ENABLED module-level constant | ignore

all **KW\_\*** above: internally-only used constants;
🏗 declare as private by prefixing an underscore

## DUMMY_OPTIONS module-level constant

A **[GlobalOptions()][GlobalOptions]** instance with default values
serving as the default value for functions or methods
expecting GlobalOptions instances

## default_excluded_module_patterns module-level function

## EmojiConfiguration class

### enabled instance attribute of configuration.EmojiConfiguration

Flag determining whether emoji should be used

### module_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing a module

### constants_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing a module-level constant

### missing_documentation_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji used to hint at missing documentation

### inheritance_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing class inheritance

### signature_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing a class, method or function signature

### class_attributes_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing a class attribute

### instance_attributes_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing an instance attribute

### property_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji symbolizing a readonly instance property

### advertisement_prefix instance attribute of configuration.EmojiConfiguration

Unicode name of the emoji prefixed to the "advertising" line

### get_serializable instance method of configuration.EmojiConfiguration

## EmojiProxy class

## GlobalOptions class

### configuration_source instance attribute of configuration.GlobalOptions

Source of the configuration
(automatically set to the configuration file path if the configuration
is loaded via the **.from_file()** method)

### source_path instance attribute of configuration.GlobalOptions

Path where python modules are searched

### excluded_modules instance attribute of configuration.GlobalOptions

Fully qualified names of the modules that
should not be included in the reference documentation

### destination_path instance attribute of configuration.GlobalOptions

Path where the output files are written

### overrides_basepath instance attribute of configuration.GlobalOptions

Path where override files are collected

### emoji instance attribute of configuration.GlobalOptions

An **[EmojiConfiguration()][EmojiConfiguration]** instance

### advertise instance attribute of configuration.GlobalOptions

Flag: append an "advertising" line at the bottom of each written page

### get_serializable instance method of configuration.GlobalOptions

### dump instance method of configuration.GlobalOptions

### from_file class method of configuration.GlobalOptions

# morast:verbatim (Morast special purpose)

[GlobalOptions]: #dataclass-globaloptions
[EmojiConfiguration]: #frozen-dataclass-emojiconfiguration
