# commandline module

This module is used to execute the command line script
in the package

## SUBCOMMAND_AUTODETECT module-level constant

Subcommand for autodetection and -documentation of modules

## SUBCOMMAND_EXTRACT module-level constant

Subcommand to extract override templates

## SUBCOMMAND_INIT module-level constant

Subcommand to initialize the .morast subdirectory

## SUBCOMMAND_SHOW_CONFIG module-level constant

Subcommand to display the configuration

## SUBCOMMAND_SINGLE_MODULE module-level constant

Subcommand to document a single module to standard output

## SUFFIX_EXTRACTED module-level constant

The suffix appended to extracted override files
if an override file for the module already exists

## RETURNCODE_OK module-level constant

Returncode signalling successful execution

## RETURNCODE_ERROR module-level constant

Returncode signalling an error

## Program class

### name class attribute of commandline.Program

The program name in the usage message

### description class attribute of commandline.Program

The program description in the usage message

### config instance attribute of commandline.Program

The global configuration is loaded
from the configuration file – if it exists –
directly after the script starts,
and kept in this attribute.

If the configuration file does not exist,
a default configuration is used (see the **configuration** module).

### arguments property of commandline.Program

### execute instance method of commandline.Program

### extract_override_templates instance method of commandline.Program

### autodetect instance method of commandline.Program

### initialize instance method of commandline.Program

### show_config instance method of commandline.Program

### single_module instance method of commandline.Program
