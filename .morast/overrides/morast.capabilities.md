# capabilities module

Generates documentation for the capabilities
implemented by a supported subset of
[special methods].

## RICH_COMPARISONS module-level constant | strip-value

[Rich comparisons]: `==`, `!=`, `<`, `>`, `<=`, and `>=`

## BASIC_METHODS module-level constant | strip-value

[Basic customization methods]

## PRX_SPECIAL_METHOD module-level constant

Regular expression pattern to recognize a special method name

## DuplicateNameError class

## NoSpecialMethodError class

## NotHandledInThisGroupError class

## UnsupportedMethodError class

## SpecialMethod class

### name instance attribute of capabilities.SpecialMethod

The name of the special method with leading
and trailing double underscores stripped

### docstring instance attribute of capabilities.SpecialMethod

The docstring of the special method

### arguments instance attribute of capabilities.SpecialMethod

The [ast.arguments] instance from the parsed function definition

### returns instance attribute of capabilities.SpecialMethod | strip-value

the _returns_ attribute from the parsed function definition

### args property of capabilities.SpecialMethod

### args_signature property of capabilities.SpecialMethod

### get_annotated_arg_item class method of capabilities.SpecialMethod

### get_nth_last_default instance method of capabilities.SpecialMethod

## BaseSignature class

### format_string class attribute of capabilities.BaseSignature

A [format string] defining how the signature will be rendered

### name instance attribute of capabilities.BaseSignature

The name of the special method

### instance_name instance attribute of capabilities.BaseSignature

The (variable) name of the instance in the documentation

### returns instance attribute of capabilities.BaseSignature

The return annotation if defined

### return_annotation property of capabilities.BaseSignature

### from_method class method of capabilities.BaseSignature

## FunctionSignature class

### format_string class attribute of capabilities.FunctionSignature

A [format string] defining how the signature will be rendered

## RichComparisonSignature class

### format_string class attribute of capabilities.RichComparisonSignature

A [format string] defining how the signature will be rendered

### comparison instance attribute of capabilities.RichComparisonSignature

The comparison operator looked up from **RICH\_COMPARISONS**
using _name_

### other_name instance attribute of capabilities.RichComparisonSignature

The other instance (variable) name extracted from the special method’s
original signature

### from_method class method of capabilities.RichComparisonSignature

## CustomFormattedSignature class

### format_string instance attribute of capabilities.CustomFormattedSignature

A [format string] defining how the signature will be rendered

### from_method class method of capabilities.CustomFormattedSignature

## Capability class

### signature instance attribute of capabilities.Capability

A [BaseSignature] subclass instance

### docstring instance attribute of capabilities.Capability

The docstring of the method that implements the capability

## SpecialMethodsGroupBase class

### handled_methods class attribute of capabilities.SpecialMethodsGroupBase

The special method names handled by this class

### instance_var property of capabilities.SpecialMethodsGroupBase

### add_method instance method of capabilities.SpecialMethodsGroupBase

### get_capability instance method of capabilities.SpecialMethodsGroupBase

### get_signature instance method of capabilities.SpecialMethodsGroupBase

### implementations instance method of capabilities.SpecialMethodsGroupBase

## BasicCustomizationGroup class

### handled_methods class attribute of capabilities.BasicCustomizationGroup

The special method names handled by this class

### get_signature instance method of capabilities.BasicCustomizationGroup

## ContainerTypesEmulationGroup class

### handled_methods class attribute of capabilities.ContainerTypesEmulationGroup | strip-value

The special method names handled by this class

### get_signature instance method of capabilities.ContainerTypesEmulationGroup

## CallableObjectsEmulationGroup class

### handled_methods class attribute of capabilities.CallableObjectsEmulationGroup

The special method names handled by this class

### get_signature instance method of capabilities.CallableObjectsEmulationGroup

## Collector class

### basic_customization class attribute of capabilities.Collector

❓ **capabilities.Collector.basic_customization** documentation _to be added_

### container_types_emulation class attribute of capabilities.Collector

❓ **capabilities.Collector.container_types_emulation** documentation _to be added_

### callable_objects class attribute of capabilities.Collector

❓ **capabilities.Collector.callable_objects** documentation _to be added_

### supported_groups class attribute of capabilities.Collector

❓ **capabilities.Collector.supported_groups** documentation _to be added_

### add_method instance method of capabilities.Collector

### as_markdown instance method of capabilities.Collector

# morast:verbatim (Morast special purpose)

[special methods]: https://docs.python.org/3/reference/datamodel.html#special-method-names
[Rich comparisons]: https://docs.python.org/3/reference/datamodel.html#object.__lt__
[Basic customization methods]: https://docs.python.org/3/reference/datamodel.html#basic-customization
[Emulating container types]: https://docs.python.org/3/reference/datamodel.html#emulating-container-types
[Emulating callable objects]: https://docs.python.org/3/reference/datamodel.html#emulating-callable-objects
[ast.arguments]: https://docs.python.org/3/library/ast.html#ast.arguments
[ast.FunctionDef]: https://docs.python.org/3/library/ast.html#ast.FunctionDef
[format string]: https://docs.python.org/3/library/string.html#formatstrings
[BaseSignature]: #class-basesignature
