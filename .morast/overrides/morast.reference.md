# reference module

High-level documentation objects

## BasePage class

### markdown_elements instance method of reference.BasePage

### render instance method of reference.BasePage

### get_extracted_overrides instance method of reference.BasePage

## MorastModule class

### kind class attribute of reference.MorastModule

Kind: module

### module_contents instance attribute of reference.MorastModule

A **[MorastAttributesList]** instance containing the
module attributes (ie. constants)

### classes instance attribute of reference.MorastModule

A **[MorastSection]** instance containing the
classes in the module

### functions instance attribute of reference.MorastModule

A **[MorastSection]** instance containing the
module-level functions

### namespaced_module property of reference.MorastModule

### from_file class method of reference.MorastModule

## LinkList class

### add_module instance method of reference.LinkList

### as_markdown instance method of reference.LinkList

## IndexPage class

### kind class attribute of reference.IndexPage

Kind: Reference index

### docstring instance attribute of reference.IndexPage

The configured or overriden docstring

### add_module instance method of reference.IndexPage

# morast:verbatim (Morast special purpose)

[MorastDocumentableItem]: morast.core.md#class-morastdocumentableitem
[MorastAttribute]: morast.core.md#class-morastattribute
[MorastAttributesList]: morast.core.md#class-morastattributeslist
[MorastBaseAttribute]: morast.core.md#class-morastbaseattribute
[MorastClassDef]: morast.core.md#class-morastclassdef
[MorastFunctionDef]: morast.core.md#class-morastfunctiondef
[MorastSection]: morast.core.md#class-morastsection
[OverridesSection]: morast.overrides.md#class-overridessection
[SuperConfig]: morast.core.md#class-superconfig
[MorastModule]: #class-morastmodule
