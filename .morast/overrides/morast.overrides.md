# overrides module

Handle override files and override sections

## ADDITIONS module-level constant

Keyword for additions:
if appended to a headline in an override file,
the following text be appended to the existing docsting
of the matched node instead of replacing it
(not implemented yet).

## IGNORE module-level constant

Keyword for ignoring a docstring:
if appended to a headline in an override file,
the matched node will be skipped in the resulting documentation.

## STRIP_VALUE module-level constant

Keyword for stripping the attribute value:
if appended to a headline in an override file,
the right-hand side of the assignment
("equals" sign and value) will not be part of the
documentation.

This processing instruction only has an effect
when applied to a constant or an attribute
(other node kinds do not imply assignments).

## KIND_CLASS_ATTRIBUTE module-level constant

Node kind for a class attribute

## KIND_CLASS_METHOD module-level constant

Node kind for a class method

## KIND_CLASS module-level constant

Node kind for a class definition

## KIND_CONSTANT module-level constant

Node kind for a module-level constant

## KIND_FUNCTION module-level constant

Node kind for a module-level function

## KIND_INSTANCE_ATTRIBUTE module-level constant

Node kind for an instance attribute

## KIND_INSTANCE_METHOD module-level constant

Node kind for an instance method

## KIND_MODULE module-level constant

Node kind for a module

## KIND_MORAST_SPECIAL module-level constant

Node kind for a Morast special-purpose section

## KIND_PROPERTY module-level constant

Node kind for an instance property _(not implemented yet)_

## KIND_REFINDEX module-level constant

Node kind for the reference index page

## KIND_UNSPECIFIED module-level constant

Unspecified node kind

## CLASS_CONTEXT_KINDS module-level constant

Node kinds in class context

## SUPPORTED_KINDS module-level constant

All supported node kinds in non-ambiguous order

## PRX_HEADLINE module-level constant

Compiled regular expression for finding headlines

## MORAST_PREFIX module-level constant

Prefix for Morast special sections

## DUMMY_MOD_OVERRIDES module-level constant

An empty **[ModuleOverrides]** instance
suitable as a default value for functions or methods requiring
an instance of that class

## OverridesSection class

### name instance attribute of overrides.OverridesSection | strip-value

The name of the overrides section

### kind instance attribute of overrides.OverridesSection | strip-value

The modee kind of the overrides section

### namespace instance attribute of overrides.OverridesSection | strip-value

The namespace of the overrides section

### additions property of overrides.OverridesSection

### docstring property of overrides.OverridesSection

### is_ignored property of overrides.OverridesSection

### value_is_stripped property of overrides.OverridesSection

### add_to_additions instance method of overrides.OverridesSection

### add_to_docstring instance method of overrides.OverridesSection

### clear_docstring instance method of overrides.OverridesSection

### ignore instance method of overrides.OverridesSection

### strip_value instance method of overrides.OverridesSection

## ModuleOverrides class

### external_namespace property of overrides.ModuleOverrides

### setdefault instance method of overrides.ModuleOverrides

### items instance method of overrides.ModuleOverrides

### get_external_namespace class method of overrides.ModuleOverrides

### from_string class method of overrides.ModuleOverrides

### from_file class method of overrides.ModuleOverrides

# morast:verbatim (Morast special purpose)

[OverridesSection]: #class-overridessection
[ModuleOverrides]: #class-moduleoverrides
[self.setdefault()]: #module_overrides_instancesetdefault
