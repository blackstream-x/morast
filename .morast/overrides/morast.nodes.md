# nodes module

Nodes definitions

## ARGS_JOINER module-level constant

❓ **nodes.ARGS_JOINER** documentation _to be added_

## EQUALS_OP module-level constant

❓ **nodes.EQUALS_OP** documentation _to be added_

## ASSIGNMENT_WIDE module-level constant

❓ **nodes.ASSIGNMENT_WIDE** documentation _to be added_

## OPENING_CURLY_BRACE module-level constant

❓ **nodes.OPENING_CURLY_BRACE** documentation _to be added_

## CLOSING_CURLY_BRACE module-level constant

❓ **nodes.CLOSING_CURLY_BRACE** documentation _to be added_

## OPENING_PARENTHESIS module-level constant

❓ **nodes.OPENING_PARENTHESIS** documentation _to be added_

## CLOSING_PARENTHESIS module-level constant

❓ **nodes.CLOSING_PARENTHESIS** documentation _to be added_

## OPENING_SQUARE_BRACKET module-level constant

❓ **nodes.OPENING_SQUARE_BRACKET** documentation _to be added_

## CLOSING_SQUARE_BRACKET module-level constant

❓ **nodes.CLOSING_SQUARE_BRACKET** documentation _to be added_

## MD_ASSIGNMENT_NARROW module-level constant

❓ **nodes.MD_ASSIGNMENT_NARROW** documentation _to be added_

## MD_ASSIGNMENT_WIDE module-level constant

❓ **nodes.MD_ASSIGNMENT_WIDE** documentation _to be added_

## MD_DICT_ITEM_JOINER module-level constant

❓ **nodes.MD_DICT_ITEM_JOINER** documentation _to be added_

## MD_ANNOTATION_JOINER module-level constant

❓ **nodes.MD_ANNOTATION_JOINER** documentation _to be added_

## MD_DOT module-level constant

❓ **nodes.MD_DOT** documentation _to be added_

## MD_STAR module-level constant

❓ **nodes.MD_STAR** documentation _to be added_

## MD_RETURN_ARROW module-level constant

❓ **nodes.MD_RETURN_ARROW** documentation _to be added_

## MD_ARGS_JOINER module-level constant

❓ **nodes.MD_ARGS_JOINER** documentation _to be added_

## MD_HR20 module-level constant

MarkDown element: horizontal rule, 20 characters wide

## MD_HR60 module-level constant

MarkDown element: horizontal rule, 60 characters wide

## OP_LOOKUP module-level constant | strip-value

Mapping of **ast** binary operator classes to their appropriate
representations, eg. **ast.Add** → `'+'`

## failsafe_mde module-level function

## get_operator_text module-level function

## get_augmentation_operator module-level function

## get_node module-level function

## remove_hanging_indent module-level function

## MorastBaseNode class

### quotes_required property of nodes.MorastBaseNode

### plain_display instance method of nodes.MorastBaseNode

### as_markdown instance method of nodes.MorastBaseNode

## MorastErrorNode class

### message instance attribute of nodes.MorastErrorNode

A message describing the error situation

### as_markdown instance method of nodes.MorastErrorNode

## MorastKeyword class

### prefix property of nodes.MorastKeyword

## MorastConstant class

### quotes_required property of nodes.MorastConstant

### as_markdown instance method of nodes.MorastConstant

## MorastFormattedValue class

### quotes_required property of nodes.MorastFormattedValue

### as_markdown instance method of nodes.MorastFormattedValue

## MorastJoinedStr class

### quotes_required property of nodes.MorastJoinedStr

### plain_display instance method of nodes.MorastJoinedStr

### as_markdown instance method of nodes.MorastJoinedStr

## MorastIfExp class

## MorastBinOp class

### left instance attribute of nodes.MorastBinOp

left side of the operation

### right instance attribute of nodes.MorastBinOp

right side of the operation

### as_markdown instance method of nodes.MorastBinOp

## MorastComprehension class

## MorastBaseComp class

### opener class attribute of nodes.MorastBaseComp

the opening character

### closer class attribute of nodes.MorastBaseComp

the closing character

## MorastListComp class

## MorastSetComp class

### opener class attribute of nodes.MorastSetComp

❓ **nodes.MorastSetComp.opener** documentation _to be added_

### closer class attribute of nodes.MorastSetComp

❓ **nodes.MorastSetComp.closer** documentation _to be added_

## MorastDictComp class

### opener class attribute of nodes.MorastDictComp

❓ **nodes.MorastDictComp.opener** documentation _to be added_

### closer class attribute of nodes.MorastDictComp

❓ **nodes.MorastDictComp.closer** documentation _to be added_

## MorastName class

## MorastNamespace class

### strip_first instance method of nodes.MorastNamespace

### as_markdown instance method of nodes.MorastNamespace

## MorastDictItem class

### as_markdown instance method of nodes.MorastDictItem

## MorastStarred class

## CompoundNode class

### prefix_node class attribute of nodes.CompoundNode

❓ **nodes.CompoundNode.prefix_node** documentation _to be added_

### opener class attribute of nodes.CompoundNode

❓ **nodes.CompoundNode.opener** documentation _to be added_

### closer class attribute of nodes.CompoundNode

❓ **nodes.CompoundNode.closer** documentation _to be added_

### joiner class attribute of nodes.CompoundNode

❓ **nodes.CompoundNode.joiner** documentation _to be added_

### add_supported class attribute of nodes.CompoundNode

❓ **nodes.CompoundNode.add_supported** documentation _to be added_

### add instance method of nodes.CompoundNode

### get_contents instance method of nodes.CompoundNode

### get_contents_md instance method of nodes.CompoundNode

### get_full_md instance method of nodes.CompoundNode

### as_markdown instance method of nodes.CompoundNode

## MorastCall class

### opener class attribute of nodes.MorastCall

❓ **nodes.MorastCall.opener** documentation _to be added_

### closer class attribute of nodes.MorastCall

❓ **nodes.MorastCall.closer** documentation _to be added_

### prefix_node instance attribute of nodes.MorastCall

❓ **nodes.MorastCall.prefix_node** documentation _to be added_

## MorastDict class

### opener class attribute of nodes.MorastDict

❓ **nodes.MorastDict.opener** documentation _to be added_

### closer class attribute of nodes.MorastDict

❓ **nodes.MorastDict.closer** documentation _to be added_

## MorastSubscript class

### opener class attribute of nodes.MorastSubscript

❓ **nodes.MorastSubscript.opener** documentation _to be added_

### closer class attribute of nodes.MorastSubscript

❓ **nodes.MorastSubscript.closer** documentation _to be added_

### prefix_node instance attribute of nodes.MorastSubscript

❓ **nodes.MorastSubscript.prefix_node** documentation _to be added_

## MorastClassBases class

### opener class attribute of nodes.MorastClassBases

❓ **nodes.MorastClassBases.opener** documentation _to be added_

### closer class attribute of nodes.MorastClassBases

❓ **nodes.MorastClassBases.closer** documentation _to be added_

### prefix_node instance attribute of nodes.MorastClassBases

❓ **nodes.MorastClassBases.prefix_node** documentation _to be added_

## MorastTuple class

### opener class attribute of nodes.MorastTuple

❓ **nodes.MorastTuple.opener** documentation _to be added_

### closer class attribute of nodes.MorastTuple

❓ **nodes.MorastTuple.closer** documentation _to be added_

### get_contents instance method of nodes.MorastTuple

### get_contents_md instance method of nodes.MorastTuple

## MorastList class

### opener class attribute of nodes.MorastList

❓ **nodes.MorastList.opener** documentation _to be added_

### closer class attribute of nodes.MorastList

❓ **nodes.MorastList.closer** documentation _to be added_

### add_supported class attribute of nodes.MorastList

❓ **nodes.MorastList.add_supported** documentation _to be added_

## MorastSet class

### opener class attribute of nodes.MorastSet

❓ **nodes.MorastSet.opener** documentation _to be added_

### closer class attribute of nodes.MorastSet

❓ **nodes.MorastSet.closer** documentation _to be added_

### add_supported class attribute of nodes.MorastSet

❓ **nodes.MorastSet.add_supported** documentation _to be added_

### get_full_md instance method of nodes.MorastSet

## Assignment class

### target instance attribute of nodes.Assignment

❓ **nodes.Assignment.target** documentation _to be added_

### prefix instance attribute of nodes.Assignment

❓ **nodes.Assignment.prefix** documentation _to be added_

### operator instance attribute of nodes.Assignment

❓ **nodes.Assignment.operator** documentation _to be added_

### annotation instance attribute of nodes.Assignment

❓ **nodes.Assignment.annotation** documentation _to be added_

### value instance attribute of nodes.Assignment

❓ **nodes.Assignment.value** documentation _to be added_

### strip_first instance method of nodes.Assignment

### as_markdown instance method of nodes.Assignment

## DocString class

### adjust_level instance method of nodes.DocString

### as_markdown instance method of nodes.DocString

## Signature class

### name instance attribute of nodes.Signature

❓ **nodes.Signature.name** documentation _to be added_

### args instance attribute of nodes.Signature

❓ **nodes.Signature.args** documentation _to be added_

### returns instance attribute of nodes.Signature

❓ **nodes.Signature.returns** documentation _to be added_

### as_markdown instance method of nodes.Signature

## Advertisement class

### as_markdown instance method of nodes.Advertisement

# morast:verbatim (Morast special purpose)

[ast.AugAssign]: https://docs.python.org/3/library/ast.html#ast.AugAssign
[ast binary operator token]: https://docs.python.org/3/library/ast.html#ast.Add
