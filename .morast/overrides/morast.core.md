# core module

Core components for AST to documentation translation

## CLASS_METHOD module-level constant

Magic number for class methods

## STATIC_METHOD module-level constant

Magic number for static methods

## INSTANCE_METHOD module-level constant

Magic number for instance methods

## MODULE_LEVEL_FUNCTION module-level constant

Magic number for module-level functions

## METHOD_TYPES module-level constant

Display names for class, statitc, and instance methods

## TYPES_BY_DECORATOR module-level constant

Mapping of method types to decorators

## SCOPE_CLASS module-level constant

Class scope (of attributes or methods)

## SCOPE_INSTANCE module-level constant

Instance scope (of attributes or methods)

## SCOPE_MODULE module-level constant

Module scope (of constants or functions)

## METHOD_TARGETS module-level constant

Mapping of scopes to method types

## EXCLUDED_MODULE_VARIABLES module-level constant

Tuple containing always excluded module variables

## PRX_DATACLASS module-level constant

Compiled regular expression for identifying the dataclass decorator

## MORAST_DOCSTRING module-level constant | strip-value

Identifier for dosctrings

## MORAST_VERBATIM module-level constant | strip-value

Identifier for verbatim sections

## DUMMY_SUPERCONFIG module-level constant

An empty **[SuperConfig]** instance suitable as default value in
class initialization arguments

## camel_to_snake_case module-level function

## IgnoredItemError class

### message instance attribute of core.IgnoredItemError | strip-value

The error message indicating why the item is ignored

## IsAPropertyError class

## SuperConfig class

### mor instance attribute of core.SuperConfig

the overrides.[ModuleOverrides]
instance from initialization

### emoji instance attribute of core.SuperConfig

an [EmojiProxy] instance built from the **emoji** attribute of the configuration
this instance was initialized with

### advertise instance attribute of core.SuperConfig

the **advertise** flag from the configuration

### get_nested_sections instance method of core.SuperConfig

### extract_overrides instance method of core.SuperConfig

## MorastDocumentableItem class

### kind class attribute of core.MorastDocumentableItem

The kind of item (used for the **kind** attribute
of an extracted overrides.[OverridesSection] instance)

### name instance attribute of core.MorastDocumentableItem | strip-value

The name of the item

### namespace instance attribute of core.MorastDocumentableItem | strip-value

The namespace of the item

### scope instance attribute of core.MorastDocumentableItem | strip-value

The scope of the item

### sc instance attribute of core.MorastDocumentableItem | strip-value

The [SuperConfig] instance containing overrides and other settings
relevant for the item

### docstring instance attribute of core.MorastDocumentableItem | strip-value

The docstring for the item

### is_ignored instance attribute of core.MorastDocumentableItem | strip-value

A Flag indicating whether the item is ignored

### check_private instance method of core.MorastDocumentableItem

### check_ignored instance method of core.MorastDocumentableItem

### set_docstring_from_override instance method of core.MorastDocumentableItem

### markdown_elements instance method of core.MorastDocumentableItem

### as_markdown instance method of core.MorastDocumentableItem

## MorastBaseAttribute class

### kind instance attribute of core.MorastBaseAttribute

The kind of the attribute or property

### markdown_elements instance method of core.MorastBaseAttribute

### as_md_list_item instance method of core.MorastBaseAttribute

## MorastAttribute class

### supported_scopes class attribute of core.MorastAttribute

Supported scopes for attributes

### assignment instance attribute of core.MorastAttribute | strip-value

The nodes.MorastAssignment instance associated with the attribute

### markdown_elements instance method of core.MorastAttribute

## MorastProperty class

### type_annotation instance attribute of core.MorastProperty | strip-value

The type annotation of the decorated function

### markdown_elements instance method of core.MorastProperty

## MorastSection class

### items instance method of core.MorastSection

### subsections instance method of core.MorastSection

### adjust_level instance method of core.MorastSection

### add_subnode instance method of core.MorastSection

### add_subsection instance method of core.MorastSection

### markdown_elements instance method of core.MorastSection

## ImplementedCapabilities class

### supported_scopes class attribute of core.ImplementedCapabilities

❓ **core.ImplementedCapabilities.supported_scopes** documentation _to be added_

### markdown_elements instance method of core.ImplementedCapabilities

## MorastAttributesList class

### supported_scopes class attribute of core.MorastAttributesList

Supported scopes for attributes lists

### add instance method of core.MorastAttributesList

### remove instance method of core.MorastAttributesList

### markdown_elements instance method of core.MorastAttributesList

## MorastFunctionDef class

### supported_scopes class attribute of core.MorastFunctionDef

Supported scopes for functions

### function_type instance attribute of core.MorastFunctionDef | strip-value

Type of the function or method

### kind instance attribute of core.MorastFunctionDef | strip-value

Kind of the function or method

## MorastClassDef class

### kind class attribute of core.MorastClassDef

Kind: class

### is_a_dataclass instance attribute of core.MorastClassDef | strip-value

Flag indicating if the class is a [dataclass][stdlib-datclass]

### existing_attributes instance attribute of core.MorastClassDef | strip-value

A mapping of sets of existing attributes by scope (class or instance)

### attribute_lists instance attribute of core.MorastClassDef | strip-value

A mapping of [MorastAttributesList] instances by scope (class or instance)

### methods instance attribute of core.MorastClassDef | strip-value

A mapping of [MorastSection] instances
(each containing a number of [MorastFunctionDef] instances)
by scope (class or instance)

# morast:verbatim (Morast special purpose)

[EmojiConfiguration]: morast.configuration.md#frozen-dataclass-emojiconfiguration
[GlobalOptions]: morast.configuration.md#dataclass-globaloptions
[ModuleOverrides]: morast.overrides.md/#class-moduleoverrides
[EmojiProxy]: morast.configuration.md#class-emojiproxy
[MorastDocumentableItem]: #class-morastdocumentableitem
[MorastAttribute]: #class-morastattribute
[MorastAttributesList]: #class-morastattributeslist
[MorastBaseAttribute]: #class-morastbaseattribute
[MorastClassDef]: #class-morastclassdef
[MorastFunctionDef]: #class-morastfunctiondef
[MorastModule]: morast.reference.md#class-morastmodule
[MorastSection]: #class-morastsection
[OverridesSection]: morast.overrides.md/#class-overridessection
[SuperConfig]: #class-superconfig
[stdlib-datclass]: https://docs.python.org/3/library/dataclasses.html
[overrides]: morast.overrides.md
