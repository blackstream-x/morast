#!/bin/bash

# Quick code check

echo -e "\n=== mypy ===\n"
python3 -m mypy . || exit
echo -e "\n=== pylint (tests) ===\n--- ignoring duplicate-code ---\n"
PYTHONPATH=src python3 -m pylint --disable=duplicate-code tests || echo "--- ignored issues in test modules ---"
echo -e "\n=== pylint (src)  ===\n"
PYTHONPATH=src python3 -m pylint --reports=y src || echo "--- ignored some issues ---"
echo -e "\n=== ruff format ===\n"
ruff format --check . || ruff format --diff .
echo -e "\n=== ruff check ===\n"
ruff check .

