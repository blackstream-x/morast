# -*- coding: utf-8 -*-

"""

tests.commons

Common functionality for Unit tests


Copyright (C) 2024 Rainer Schwarzbach

This file is part of morast.

morast is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

morast is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""

import ast
import dataclasses
import io
import sys

from typing import Iterator

from unittest.mock import patch


RETURNCODE_OK = 0


def parsed_body(source: str) -> Iterator[ast.AST]:
    """Return an iterator over the body elements of parsed _source_"""
    parsed_module = ast.parse(source)
    for item in parsed_module.body:
        if isinstance(item, ast.AST):
            yield item
        #
    #


def first_parsed_body_element(source: str) -> ast.AST:
    """Return first element in the body of parsed _source_"""
    for element in parsed_body(source):
        return element
    #
    raise ValueError(f"No AST element found in {source!r}")


@dataclasses.dataclass(frozen=True)
class GenericCallResult:
    """Result from a generic call"""

    returncode: int = RETURNCODE_OK
    stdout: str = ""

    @classmethod
    def do_call(cls, *args, **kwargs):
        """Abstract method: do the real function call"""
        raise NotImplementedError

    @classmethod
    def from_call(
        cls,
        *arguments,
        stdin_data=None,
        stdout=sys.stdout,
        stderr=sys.stderr,
        **kwargs,
    ):
        """Return a GenericCallResult instance
        from the real function call,
        mocking sys.stdin if stdin_data was provided.
        """
        assert stdout is sys.stdout
        assert stderr is sys.stderr
        if stdin_data is None:
            returncode = cls.do_call(*arguments, **kwargs)
        else:
            with patch("sys.stdin", new=io.StringIO(stdin_data)) as mock_stdin:
                assert mock_stdin is sys.stdin
                returncode = cls.do_call(*arguments, **kwargs)
            #
        #
        return cls(
            returncode=returncode,
            stdout=stdout.getvalue(),
        )


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
