# Abstract

![Münchhausen rescues himself from the swamp using his own braids][muenchhausen-swamp]{ align=right }


This package creates reference documentation from Python source modules.

Its name,

> **Morast** /moˈʁast/ _(German: morass, mud, quagmire, mire, bog)_,

is an acronym of
_**Mo**dule **r**eference by **a**nalyzing the **s**yntax **t**ree_,
but also serves as a pun referencing the story of [Baron Münchhausen]
rescuing himself and his horse from drowning in a swamp by
pulling both out by his own hair.(1)
{ .annotate }

1. That anecdote can be found (in german) at Wikisource:
   [first part][the swamp story], [second part][the swamp story continued].

_(image modified from [Theodor Hosemann (1807-1875)][munechhhausen-image-wikimedia-commons], Public domain, via Wikimedia Commons)_

---

The executable analyzes your Python sources using the [Abstract Syntax Trees][ast]
module from Python’s standard library, extracts the [docstrings][docs-docstrings],
pulls in user-generated overrides
(written in MarkDown with some implied assumptions, see the
[Overrides documentation section][docs-overrides])
and creates a reference documentation in MarkDown (which in turn can be
post-processed into other formats using eg. [MkDocs][MkDocs]).

The configuration is saved in a YAML file in a subdirectory named `.morast`.

![Mindmap (generated using Mermaid): Morast concepts][concepts-mindmap]


## Installation

**Morast** is available on PyPI: <https://pypi.org/project/morast/>

```
pip install morast
```

Installation in a virtual environment is strongly recommended.


## Usage

Output of `python -m morast --help`:

```text
usage: morast [-h] [--version] [-d | -v | -q]
              {auto,extract,init,config,module} ...

Create reference documentation from sources using AST

positional arguments:
  {auto,extract,init,config,module}
    auto                automatically detect and document all modules
    extract             extract override templates from modules
    init                initialize the Morast project
    config              show the configuration
    module              document a single module

options:
  -h, --help            show this help message and exit
  --version             print version and exit

Logging options:
  control log level (default is WARNING)

  -d, --debug           output all messages (log level DEBUG)
  -v, --verbose         be more verbose (log level INFO)
  -q, --quiet           be more quiet (log level ERROR)
```

<div class="grid cards" markdown>

- :black_small_square:
- **Next: [Usage scenarios][docs-scenarios]** :arrow_right:

</div>

[ast]: https://docs.python.org/3/library/ast.html
[Baron Münchhausen]: https://en.wikipedia.org/wiki/Baron_Munchausen
[the swamp story]: https://de.wikisource.org/wiki/Seite:Abentheuer_des_Freyherrn_von_Muenchhausen_(1786).djvu/62
[the swamp story continued]: https://de.wikisource.org/wiki/Seite:Abentheuer_des_Freyherrn_von_Muenchhausen_(1786).djvu/65
[muenchhausen-swamp]: assets/muenchhausen-swamp.png "Münchhausen rescues himself from the swamp using his own braids"
[munechhhausen-image-wikimedia-commons]: https://commons.wikimedia.org/wiki/File:M%C3%BCnchhausen-Sumpf-Hosemann.png
[concepts-mindmap]: assets/concepts-mindmap.png
[docs-scenarios]: ./2-usage.md
[docs-docstrings]: ./3-docstrings.md
[docs-overrides]: ./4-overrides.md
[MkDocs]: https://www.mkdocs.org/
