# 🧩 demo

>
> demo.py
>
> Simple functions for Morast and doctest demonstration

--------------------

## Functions

--------------------

### Function: square()

> ------------------------------------------------------------
>
> 📖 demo.**square**(_number_: int) → int
>
> ------------------------------------------------------------

> Return the result of _number_ squared
>
> Examples:
>
>     >>> square(2)
>     4
>     >>> square(5)
>     25
>     >>> square(7)
>     49
>     >>>

--------------------

### Function: sumup()

> ------------------------------------------------------------
>
> 📖 demo.**sumup**(_number_: int) → int
>
> ------------------------------------------------------------

> Return the sum of 1 up to _number_
>
> Examples:
>
>     >>> sumup(2)
>     3
>     >>> sumup(5)
>     15
>     >>>
>
> Failing example:
>
>     >>> sumup(7)
>     19
>     >>>

