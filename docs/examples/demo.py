# -*- coding: utf-8 -*-
"""
demo.py

Simple functions for Morast and doctest demonstration
"""


def square(number: int) -> int:
    """Return the result of _number_ squared

    Examples:

        >>> square(2)
        4
        >>> square(5)
        25
        >>> square(7)
        49
        >>>

    """
    return number * number


def sumup(number: int) -> int:
    """Return the sum of 1 up to _number_

    Examples:

        >>> sumup(2)
        3
        >>> sumup(5)
        15
        >>>

    Failing example:

        >>> sumup(7)
        19
        >>>
    """
    return (square(number) + number) // 2


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
