# Writing docstrings

## General instructions

For use with **Morast**, you just write plain MarkDown
in the docstrings.

Indentation of following lines is fixed automatically.

Please note that docstrings are normal strings in Python,
so you either have to mask literal backslashes (`\`)
by prefixing another backslash (`\\`) or by declaring the whole docstring as raw
(`r"""docstring"""`).

When analyzing classes, the contents of the `__init__()` method’s docstring
are appended to the class docstring’s contents (separated by a blank line)
so you can write the arguments documentation close to the source code
it refers to.


## Extra indentation

Extra indentation in docstrings (ie. indentation beyond the normal hanging indent)
is preserved so you can simply write MarkDown codeblocks this way.

## Usage with doctest

Use extra indentation to mark the code sections as docstrings, for example:

    def square(number: int) -> int:
        """Square _number_

        Example usage:

            >>> square(5)
            25
            >>>
        """
        return number * number

!!! tip

    Remember to use either raw docstrings or mask special characters
    using double backslashes (eg. newline characters as `\\n`).


## Longer example

Using [examples/demo.py][examples demo source]
(the file contains an intended documentation error in the
last example of the **sumup** function)

**doctest results**

```text
$ python3 -m doctest -v demo.py
Trying:
    square(2)
Expecting:
    4
ok
Trying:
    square(5)
Expecting:
    25
ok
Trying:
    square(7)
Expecting:
    49
ok
Trying:
    sumup(2)
Expecting:
    3
ok
Trying:
    sumup(5)
Expecting:
    15
ok
Trying:
    sumup(7)
Expecting:
    19
**********************************************************************
File "…/examples/demo.py", line 39, in demo.sumup
Failed example:
    sumup(7)
Expected:
    19
Got:
    28
1 items had no tests:
    demo
1 items passed all tests:
   3 tests in demo.square
**********************************************************************
1 items had failures:
   1 of   3 in demo.sumup
6 tests in 3 items.
5 passed and 1 failed.
***Test Failed*** 1 failures.
$
```

**morast module results** (→ [rendered display][examples demo rendering])

```text
$ python -m morast module demo.py
# 🧩 demo

>
> demo.py
>
> Simple functions for Morast and doctest demonstration

--------------------

## Functions

--------------------

### Function: square()

> ------------------------------------------------------------
>
> 📖 demo.**square**(_number_: int) → int
>
> ------------------------------------------------------------

> Return the result of _number_ squared
>
> Examples:
>
>     >>> square(2)
>     4
>     >>> square(5)
>     25
>     >>> square(7)
>     49
>     >>>

--------------------

### Function: sumup()

> ------------------------------------------------------------
>
> 📖 demo.**sumup**(_number_: int) → int
>
> ------------------------------------------------------------

> Return the sum of 1 up to _number_
>
> Examples:
>
>     >>> sumup(2)
>     3
>     >>> sumup(5)
>     15
>     >>>
>
> Failing example:
>
>     >>> sumup(7)
>     19
>     >>>

$
```


<div class="grid cards" markdown>

- :arrow_left: **Previous: [Usage scenarios]**
- **Next: [Overrides]** :arrow_right:

</div>

[Usage scenarios]: ./2-usage.md
[Overrides]: ./4-overrides.md

[examples demo source]: ./examples/demo.py
[examples demo rendering]: ./examples/demo.md
