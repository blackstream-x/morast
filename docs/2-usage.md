# Usage scenarios

## Single-module

Here, you just specify the module name to extract the documentation from
on the command line after `morast module`, and its output is printed
to standard output:

```text
(venv) user@host:/tmp/standalone$ tree -anF $PWD
/tmp/standalone/
└── simple.py

1 directory, 1 file
(venv) user@host:/tmp/standalone$ cat simple.py
# -*- coding: utf-8 -*-

"""simple module for demonstration purposes"""

MY_VERSION = "1.0"


def helloworld():
    """Print a "hello. world" message"""
    print("hello, world!")

(venv) user@host:/tmp/standalone$ python -m morast module simple.py
# 🧩 simple

> simple module for demonstration purposes

## Module contents

*   📌 simple.**MY\_VERSION** = `'1.0'`

    > 🚧 **simple.MY_VERSION** documentation _to be added_

--------------------

## Functions

--------------------

### Function: helloworld()

> ------------------------------------------------------------
>
> 📖 simple.**helloworld**()
>
> ------------------------------------------------------------

> Print a "hello. world" message

(venv) user@host:/tmp/standalone$
```

If a configuration is found in **.morast/config.yaml**,
that configuration will be used, else the default configuration.
No files will be written, and no override files will be used.


## Package workflow

*   step 1 (once): initialize a new **Morast** project using `morast init`
*   step 2: review the configuration using `morast config` or by examining the file
    **.morast/config.yaml** and customize to your needs.
*   step 3: extract override templates using `morast extract`.
*   step 4: review and customize the extracted overrides.
*   step 5: build reference documentation using `morast auto`
*   After module changes, you can start at step 3 again.
    In the case of override-relevant changes, the newly extracted override templates
    will have a `+extracted` name suffix so you can examine the differences
    between your customized overrides and the newly extracted templates.

In the following sections, we will discuss how the different
commands will affect the file and directory structure of
an example python package project with the following initial structure:

```text
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── LICENSE
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        └── text.py

3 directories, 6 files
(venv) user@host:/tmp/package$
```


### subcommand: init

`morast init` will – if it does not exist yet - create a subdirectory
**.morast** in the current directory, place a new **config.yaml** file there,
and create an **overrides** subdirectory beneath the newly-created **.morast**
directory.

```text
(venv) user@host:/tmp/package$ python -m morast init
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── LICENSE
├── .morast/
│   ├── config.yaml
│   └── overrides/
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        └── text.py

5 directories, 7 files
(venv) user@host:/tmp/package$
```


### subcommand: config

`morast config` just prints out the configuration as a YAML dump,
with a leading comment line denoting the source (configuration file or default).

```text
(venv) user@host:/tmp/package$ python -m morast config
# Source: configuration file .morast/config.yaml
source_path: src
excluded_modules:
- '*.__*__'
destination_path: docs/reference
overrides_basepath: .morast/overrides
emoji:
  enabled: true
  module_prefix: JIGSAW PUZZLE PIECE
  constants_prefix: PUSHPIN
  missing_documentation_prefix: CONSTRUCTION SIGN
  inheritance_prefix: HATCHING CHICK
  signature_prefix: OPEN BOOK
  class_attributes_prefix: ROUND PUSHPIN
  instance_attributes_prefix: PAPERCLIP
  advertisement_prefix: PUBLIC ADDRESS LOUDSPEAKER
advertise: false

(venv) user@host:/tmp/package$ cat .morast/config.yaml
source_path: src
excluded_modules:
- '*.__*__'
destination_path: docs/reference
overrides_basepath: .morast/overrides
emoji:
  enabled: true
  module_prefix: JIGSAW PUZZLE PIECE
  constants_prefix: PUSHPIN
  missing_documentation_prefix: CONSTRUCTION SIGN
  inheritance_prefix: HATCHING CHICK
  signature_prefix: OPEN BOOK
  class_attributes_prefix: ROUND PUSHPIN
  instance_attributes_prefix: PAPERCLIP
  advertisement_prefix: PUBLIC ADDRESS LOUDSPEAKER
advertise: false
(venv) user@host:/tmp/package$
```


### subcommand: extract

`morast extract` extracts a documentation template for each non-excluded module
(ie. each one not matching any of the exclusion patterns defined through
the configration option list **excluded\_modules**).

```text
(venv) user@host:/tmp/package$ python -m morast extract
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── LICENSE
├── .morast/
│   ├── config.yaml
│   └── overrides/
│       ├── example.numbers.md
│       └── example.text.md
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        └── text.py

5 directories, 9 files
(venv) user@host:/tmp/package$
```


### subcommand: auto

`morast auto` generates documentation for each module in the configured
**destination\_path** if that exists. Otherwise, it prints an error message:

```text
(venv) user@host:/tmp/package$ python -m morast auto
ERROR    | The configured destination path docs/reference does not exist (yet).
(venv) user@host:/tmp/package$
(venv) user@host:/tmp/package$ mkdir -p docs/reference
(venv) user@host:/tmp/package$ python -m morast auto
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── docs/
│   └── reference/
│       ├── example.numbers.md
│       └── example.text.md
├── LICENSE
├── .morast/
│   ├── config.yaml
│   └── overrides/
│       ├── example.numbers.md
│       └── example.text.md
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        └── text.py

7 directories, 11 files
(venv) user@host:/tmp/package$
```

The generated files in the destination path contain the final
reference documentation from the module with overrides applied.


### new extract call after module changes

If a module has changed,
a new `morast extract` call will place the newly extracted template
into a file with a suffix `+extracted` appended to the module name
s you can examine the differences:

```text
(venv) user@host:/tmp/package$ vim src/example/text.py
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── docs/
│   └── reference/
│       ├── example.numbers.md
│       └── example.text.md
├── LICENSE
├── .morast/
│   ├── config.yaml
│   └── overrides/
│       ├── example.numbers.md
│       └── example.text.md
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        ├── text.py
        └── text.py.bak

7 directories, 12 files
(venv) user@host:/tmp/package$ diff src/example/text.py.bak src/example/text.py
13a14,16
> CONSTANT = "dummy"
>
>
(venv) user@host:/tmp/package$
(venv) user@host:/tmp/package$ python -m morast extract
(venv) user@host:/tmp/package$ tree -anF $PWD
/tmp/package/
├── docs/
│   └── reference/
│       ├── example.numbers.md
│       └── example.text.md
├── LICENSE
├── .morast/
│   ├── config.yaml
│   └── overrides/
│       ├── example.numbers.md
│       ├── example.text+extracted.md
│       └── example.text.md
├── pyproject.toml
├── README.md
└── src/
    └── example/
        ├── __init__.py
        ├── numbers.py
        ├── text.py
        └── text.py.bak

7 directories, 13 files
(venv) user@host:/tmp/package$
(venv) user@host:/tmp/package$ diff .morast/overrides/example.text.md .morast/overrides/example.text+extracted.md
2a3,6
> ## CONSTANT module-level constant
>
> 🚧 **text.CONSTANT** documentation _to be added_
>
(venv) user@host:/tmp/package$
```


The `*+extracted.md` files should normally not be checked in to your version control system,
they are merely provided as little helpers to transport module changes into
your permanent (and ideally VCS-tracked) override files.

You can exclude the  `*+extracted.md` files eg. by the following line
in your **.gitignore** file:

```text
.morast/overrides/*+extracted.md
```


<div class="grid cards" markdown>

- :arrow_left: **Previous: [Abstract]**
- **Next: [Writing Docstrings]** :arrow_right:

</div>


[Abstract]: ./index.md
[Writing Docstrings]: ./3-docstrings.md
